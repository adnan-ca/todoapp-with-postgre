# README #


### What is this repository for? ###

* A Todo App using Django and PostgreSQL. It presents a user to add or delete tasks in a todo list.

### How do I get set up? ###

* To clone the repository to your local machine go to Desktop using Terminal and run
    + git clone https://adnan-ca@bitbucket.org/adnan-ca/todoapp-with-postgre.git
* To install django using terminal Run
    + pip install django
* Install PostgreSQL and PgAdmin
* Start PostgreSql server on your local machine and either edit settings.py or create a database using PgAdmin with following credentials:
    + Name = TodoDB
    + User = postgres
    + Password = postgres
* Go to the cloned directory  using the terminal and Run
    + pip install psycopg2-binary
    + python manage.py makemigrations
    + python manage.py migrate
    + python manage.gy runserver
* To create a superuser Run
    + python manage.py createsuperuser


### How to use ###

* Go to localhost:8000/todos/list
* Write the name of the task in the textfield and click on the Plus icon to create a new task
* To delete a listed task click the attached delete icon
* Go to admin page using localhost:8000/admin

